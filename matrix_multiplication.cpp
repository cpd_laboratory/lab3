#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define SIZE 10

int main(void)
{

	float A[SIZE][SIZE], B[SIZE][SIZE], C[SIZE][SIZE], total;
	int i, j, k, tid;
	total = 0.0;
	for (i = 0; i < SIZE; i++)
	{
		for (j = 0; j < SIZE; j++) {
			A[i][j] = (j + 1) * 1.0;
			B[i][j] = (j + 1) * 1.0;
			C[i][j] = 0.0;
		}
	}

	printf("Starting values of matrix A:\n");
	for (i = 0; i < SIZE; i++)
	{
		for (j = 0; j < SIZE; j++) {
			printf("%.1f ", A[i][j]);
		}		
        printf("\n");	
	}

	printf("Starting values of matrix B:\n");
	for (i = 0; i < SIZE; i++)
	{
		for (j = 0; j < SIZE; j++) {
			printf("%.1f ", B[i][j]);
		}
		printf("\n");
	}

	printf("Results by thread/row:\n");
	
	/* Create a team of threads and scope variables */

	omp_set_num_threads(8);
	omp_set_nested(1);
	double start_time = omp_get_wtime();
    #pragma omp parallel shared(A,B,C,total) private(tid,i,j,k)
	{
        tid = omp_get_thread_num();
        printf("#%d thread working...\n", tid);
        

        #pragma omp for private(i, j, k)
        for (i = 0; i < SIZE; i++)
        {
            for (k = 0; k < SIZE; k++)
            {
                for (j = 0; j < SIZE; j++)
                {
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }

	}
	double time = omp_get_wtime() - start_time;
	printf("Total time:%f\n", time);

	printf("Elements of C:\n");
	for (i = 0; i < SIZE; i++)
	{
		for (j = 0; j < SIZE; j++) {
			printf("%.1f ", C[i][j]);
		}
		printf("\n");
	}
}